package com.pokemonshowdown.app;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pokemonshowdown.data.ItemDex;
import com.pokemonshowdown.data.MoveDex;
import com.pokemonshowdown.data.Pokedex;
import com.pokemonshowdown.data.Pokemon;
import com.pokemonshowdown.data.PokemonInfo;
import com.pokemonshowdown.data.RunWithNet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Set;

public class PokemonInfoFragment extends DialogFragment {
    public final static String PTAG = PokemonInfoFragment.class.getName();
    public final static String POKEMON_INFO = "PokemonInfo";
    public final static String SWITCH = "Switch";
    public final static String FRAGMENT_TAG = "Fragment Tag";
    public final static String ID = "Id";

    private PokemonInfo mPokemonInfo;
    private boolean mSwitch;
    private String mFragmentTag;
    private int mId;

    public PokemonInfoFragment() {
        // Required empty public constructor
    }

    public static PokemonInfoFragment newInstance(PokemonInfo pkm, boolean switchPkm) {
        PokemonInfoFragment fragment = new PokemonInfoFragment();
        Bundle args = new Bundle();
        args.putSerializable(POKEMON_INFO, pkm);
        args.putBoolean(SWITCH, switchPkm);
        fragment.setArguments(args);
        return fragment;
    }

    public static PokemonInfoFragment newInstance(PokemonInfo pkm, boolean switchPkm, String tag, int id) {
        PokemonInfoFragment fragment = new PokemonInfoFragment();
        Bundle args = new Bundle();
        args.putSerializable(POKEMON_INFO, pkm);
        args.putBoolean(SWITCH, switchPkm);
        args.putString(FRAGMENT_TAG, tag);
        args.putInt(ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPokemonInfo = (PokemonInfo) getArguments().getSerializable(POKEMON_INFO);
        mSwitch = getArguments().getBoolean(SWITCH);
        mFragmentTag = getArguments().getString(FRAGMENT_TAG);
        mId = getArguments().getInt(ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pokemon_info, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        int[] typeIcon = mPokemonInfo.getTypeIcon();
        int type2;
        if (typeIcon.length == 2) {
            type2 = typeIcon[1];
        } else {
            type2 = 0;
        }

        TextView pokemonName = (TextView) view.findViewById(R.id.pokemon_name);
        pokemonName.setText(mPokemonInfo.getName());
        pokemonName.setCompoundDrawablesWithIntrinsicBounds(mPokemonInfo.getIcon(getActivity()), 0, typeIcon[0], 0);
        pokemonName.setCompoundDrawablePadding(8);

        TextView pokemonWeakness = (TextView) view.findViewById(R.id.pkm_weakness);
        pokemonWeakness.setText("This is a test.");
        try {
            JSONObject jsonObject = Pokedex.get(getActivity()).getPokemonJSONObject(mPokemonInfo.getName());
            JSONArray types = jsonObject.getJSONArray("types");
            if (types.length() == 2) {
                pokemonWeakness.setText(types.get(0).toString() + " - " + types.get(1).toString());
            } else {
                pokemonWeakness.setText(types.get(0).toString());
            }
        } catch (JSONException e) {
            pokemonWeakness.setVisibility(View.INVISIBLE);
        }

        TextView pokemonLevelGender = (TextView) view.findViewById(R.id.pokemon_level_gender);
        pokemonLevelGender.setText("Lv " + mPokemonInfo.getLevel());
        pokemonLevelGender.setCompoundDrawablesWithIntrinsicBounds(type2, 0, Pokemon.getGenderIcon(mPokemonInfo.getGender()), 0);
        pokemonLevelGender.setCompoundDrawablePadding(8);

        ImageView pokemonView = (ImageView) view.findViewById(R.id.pokemon_view);
        pokemonView.setImageResource(mPokemonInfo.getSprite(getActivity()));

        TextView pokemonStats = (TextView) view.findViewById(R.id.stats);
        pokemonStats.setText(getStatsString());

        TextView pokemonAbility = (TextView) view.findViewById(R.id.stats_abilities);
        pokemonAbility.setText(mPokemonInfo.getAbilityName(getActivity()));

        TextView nature = (TextView) view.findViewById(R.id.nature);
        if (mPokemonInfo.getNature() != null) {
            nature.setText(mPokemonInfo.getNature());
        } else {
            nature.setVisibility(View.GONE);
        }

        TextView item = (TextView) view.findViewById(R.id.item);
        if (mPokemonInfo.getItemName(getActivity()) != null) {
            item.setText(mPokemonInfo.getItemName(getActivity()));
            item.setCompoundDrawablesWithIntrinsicBounds(ItemDex.getItemIcon(getActivity(), mPokemonInfo.getItemName(getActivity())), 0, 0, 0);
        } else {
            item.setVisibility(View.GONE);
        }

        TextView status = (TextView) view.findViewById(R.id.status);
        if (mPokemonInfo.getStatus() != null) {
            setStatus(status, mPokemonInfo.getStatus());
        } else {
            status.setVisibility(View.GONE);
        }

        TextView hp = (TextView) view.findViewById(R.id.hp);
        hp.setText(Integer.toString(mPokemonInfo.getHp()));

        ProgressBar hpBar = (ProgressBar) view.findViewById(R.id.bar_hp);
        hpBar.setProgress(mPokemonInfo.getHp());

        HashMap<String, Integer> moves = mPokemonInfo.getMoves();
        Set<String> moveSets = moves.keySet();
        String[] moveNames = moveSets.toArray(new String[moveSets.size()]);
        if (moveNames.length < 4) {
            view.findViewById(R.id.move4).setVisibility(View.GONE);
        } else {
            ((TextView) view.findViewById(R.id.move4_name)).setText(MoveDex.getMoveName(getActivity(), moveNames[3]));
            ((ImageView) view.findViewById(R.id.move4_type)).setImageResource(MoveDex.getMoveTypeIcon(getActivity(), moveNames[3]));
            ((TextView) view.findViewById(R.id.move4_pp)).setText(Integer.toString(moves.get(moveNames[3])));
            view.findViewById(R.id.move4).setOnClickListener(parseMoveInfoRequest(moveNames[3]));
        }

        if (moveNames.length < 3) {
            view.findViewById(R.id.move3).setVisibility(View.GONE);
        } else {
            ((TextView) view.findViewById(R.id.move3_name)).setText(MoveDex.getMoveName(getActivity(), moveNames[2]));
            ((ImageView) view.findViewById(R.id.move3_type)).setImageResource(MoveDex.getMoveTypeIcon(getActivity(), moveNames[2]));
            ((TextView) view.findViewById(R.id.move3_pp)).setText(Integer.toString(moves.get(moveNames[2])));
            view.findViewById(R.id.move3).setOnClickListener(parseMoveInfoRequest(moveNames[2]));
        }

        if (moveNames.length < 2) {
            view.findViewById(R.id.move2).setVisibility(View.GONE);
        } else {
            ((TextView) view.findViewById(R.id.move2_name)).setText(MoveDex.getMoveName(getActivity(), moveNames[1]));
            ((ImageView) view.findViewById(R.id.move2_type)).setImageResource(MoveDex.getMoveTypeIcon(getActivity(), moveNames[1]));
            ((TextView) view.findViewById(R.id.move2_pp)).setText(Integer.toString(moves.get(moveNames[1])));
            view.findViewById(R.id.move2).setOnClickListener(parseMoveInfoRequest(moveNames[1]));
        }

        if (moveNames.length < 1) {
            view.findViewById(R.id.move1).setVisibility(View.GONE);
        } else {
            ((TextView) view.findViewById(R.id.move1_name)).setText(MoveDex.getMoveName(getActivity(), moveNames[0]));
            ((ImageView) view.findViewById(R.id.move1_type)).setImageResource(MoveDex.getMoveTypeIcon(getActivity(), moveNames[0]));
            ((TextView) view.findViewById(R.id.move1_pp)).setText(Integer.toString(moves.get(moveNames[0])));
            view.findViewById(R.id.move1).setOnClickListener(parseMoveInfoRequest(moveNames[0]));
        }

        ImageView switchPkm = (ImageView) view.findViewById(R.id.switchPkm);
        if (mSwitch) {
            switchPkm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new RunWithNet() {
                        @Override
                        public void runWithNet() throws Exception {
                            switchPkm();
                        }
                    }.run();
                }
            });
        } else {
            switchPkm.setVisibility(View.GONE);
        }
    }

    private View.OnClickListener parseMoveInfoRequest(final String moveName) {

        if (getView() == null)
            return null;

        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                try {
//                    JSONObject move = MoveDex.get(getActivity().getApplication()).getMoveJsonObject(moveName);
//                    //if (move.getString("category") != "Status")
//                    Dialog dialog = new AlertDialog.Builder(getActivity())
//                            .setTitle(move.getString("name"))
//                            .setMessage(
//                                    MessageFormat.format(
//                                            "Max PP: {1}\n" +
//                                            "Base power: {2}\n" +
//                                            "Accuracy: {3}\n" +
//                                            "Priority: {4}\n" +
//                                            "Category: {5}\n" +
//                                            "Effect:\n{6}",
//                                            /*moveJson.getInt("pp")*/"?", /*moveJson.getString("maxpp")*/move.getString("pp"),
//                                            move.getString("basePower"),
//                                            move.getInt("accuracy") == 1 ? "100" : move.getInt("accuracy"),
//                                            move.getInt("priority"),
//                                            move.getString("category"),
//                                            move.getString("desc")))
//                            .setIcon(MoveDex.getTypeIcon(getActivity().getApplication(), move.getString("type")))
//                            .create();
//                    dialog.show();
//                } catch (JSONException e) {
//                    return;
//                }
                MoveInfoFragment.showMoveInformation(getActivity(), moveName);
                return;
            }
        };
    }

    public String getStatsString() {
        int[] stats = mPokemonInfo.getStats();
        return ("Atk " + stats[0] + " / Def " + stats[1] + " / SpA " + stats[2] + " / SpD " + stats[3] + " / Spe " + stats[4]);
    }

    public void setStatus(TextView statusView, String status) {
        if (getView() == null) {
            return;
        }

        statusView.setText(status.toUpperCase());
        switch (status) {
            case "slp":
                statusView.setBackgroundResource(R.drawable.editable_frame_blackwhite);
                break;
            case "psn":
            case "tox":
                statusView.setBackgroundResource(R.drawable.editable_frame_light_purple);
                break;
            case "brn":
                statusView.setBackgroundResource(R.drawable.editable_frame_light_red);
                break;
            case "par":
                statusView.setBackgroundResource(R.drawable.editable_frame_light_orange);
                break;
            case "frz":
                statusView.setBackgroundResource(R.drawable.editable_frame);
                break;
            default:
                statusView.setBackgroundResource(R.drawable.editable_frame);
        }
        statusView.setPadding(2, 2, 2, 2);
    }

    private void switchPkm() throws JSONException {
        BattleFieldFragment battleFieldFragment = (BattleFieldFragment) getActivity().getSupportFragmentManager()
                .findFragmentByTag(BattleFieldActivity.BATTLE_FIELD_FRAGMENT_TAG);
        BattleFragment fragment = (BattleFragment) battleFieldFragment.getChildFragmentManager()
                .findFragmentByTag(mFragmentTag);
        if (fragment != null) {
            fragment.processSwitch(mId);
        }
        this.dismiss();
    }

}
