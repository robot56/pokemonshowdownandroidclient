package com.pokemonshowdown.app;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

//import com.felipecsl.gifimageview.library.GifImageView;
import com.koushikdutta.ion.Ion;
import com.pokemonshowdown.application.MyApplication;
import com.pokemonshowdown.data.BattleMessage;
import com.pokemonshowdown.data.Pokemon;
import com.pokemonshowdown.data.PokemonInfo;
import com.pokemonshowdown.data.RunWithNet;
import com.pokemonshowdown.app.BattleFragment;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class TeamPreviewFragment extends BattleMessage {

    public void startTeamPreview(final BattleFragment battleFragment, final ArrayList<PokemonInfo> team1, final ArrayList<PokemonInfo> team2, final int teamSelectionSizeFinal) {
        battleFragment.getActivity().runOnUiThread(new RunWithNet() {
            @Override
            public void runWithNet() {
                if (battleFragment.getView() == null) {
                    return;
                }

                FrameLayout frameLayout = (FrameLayout) battleFragment.getView().findViewById(R.id.battle_interface);
                frameLayout.removeAllViews();
                battleFragment.getActivity().getLayoutInflater().inflate(R.layout.fragment_battle_teampreview, frameLayout);
                for (int i = 0; i < team1.size(); i++) {
                    ImageView sprites = (ImageView) battleFragment.getView().findViewById(battleFragment.getTeamPreviewSpriteId("p1", i));
                    PokemonInfo pkm = team1.get(i);
                    sprites.setImageResource(Pokemon.getPokemonSprite(battleFragment.getActivity(),
                            MyApplication.toId(pkm.getName()), true, pkm.isFemale(), pkm.isShiny()));
                    //patch
                    Pokemon.getPokemonSpriteAnimatedRemoteURL(battleFragment.getActivity(), pkm.getName(), true, pkm.isFemale(), pkm.isShiny());
                    //end patch
                    ((ImageView) battleFragment.getView().findViewById(battleFragment.getIconId("p1", i)))
                            .setImageResource(Pokemon.getPokemonIcon(battleFragment.getActivity(),
                                    MyApplication.toId(pkm.getName())));
                }
                for (int i = team1.size(); i < 6; i++) {
                    ((ImageView) battleFragment.getView().findViewById(battleFragment.getIconId("p1", i)))
                            .setImageResource(R.drawable.pokeball_none);
                }
                for (int i = 0; i < team2.size(); i++) {
                    ImageView sprites = (ImageView) battleFragment.getView().findViewById(battleFragment.getTeamPreviewSpriteId("p2", i));
                    PokemonInfo pkm = team2.get(i);
                    sprites.setImageResource(Pokemon.getPokemonSprite(battleFragment.getActivity(),
                            MyApplication.toId(pkm.getName()), false, pkm.isFemale(), pkm.isShiny()));
                    ((ImageView) battleFragment.getView().findViewById(battleFragment.getIconId("p2", i)))
                            .setImageResource(Pokemon.getPokemonIcon(battleFragment.getActivity(),
                                    MyApplication.toId(pkm.getName())));
                }
                for (int i = team2.size(); i < 6; i++) {
                    ((ImageView) battleFragment.getView().findViewById(battleFragment.getIconId("p2", i)))
                            .setImageResource(R.drawable.pokeball_none);
                }
                battleFragment.setTeamSize(teamSelectionSizeFinal);
                battleFragment.getView().findViewById(R.id.p2a_prev)
                        .setOnClickListener(battleFragment.new PokemonInfoListener(false, 0));
                battleFragment.getView().findViewById(R.id.p2b_prev)
                        .setOnClickListener(battleFragment.new PokemonInfoListener(false, 1));
                battleFragment.getView().findViewById(R.id.p2c_prev)
                        .setOnClickListener(battleFragment.new PokemonInfoListener(false, 2));
                battleFragment.getView().findViewById(R.id.p2d_prev)
                        .setOnClickListener(battleFragment.new PokemonInfoListener(false, 3));
                battleFragment.getView().findViewById(R.id.p2e_prev)
                        .setOnClickListener(battleFragment.new PokemonInfoListener(false, 4));
                battleFragment.getView().findViewById(R.id.p2f_prev)
                        .setOnClickListener(battleFragment.new PokemonInfoListener(false, 5));

                if (battleFragment.getAnimatorSetQueue().isEmpty() && battleFragment.getRequestJson() != null) {
                    battleFragment.startRequest();
                }

            }
        });
    }

    public void stopTeamPreview(final BattleFragment battleFragment) {
        FrameLayout frameLayout = (FrameLayout) battleFragment.getView().findViewById(R.id.battle_interface);
        frameLayout.removeAllViews();
    }

    public void startAnimatedTeamPreview(final BattleFragment battleFragment, final ArrayList<PokemonInfo> team1, final ArrayList<PokemonInfo> team2, final int teamSelectionSizeFinal) {
        battleFragment.getActivity().runOnUiThread(new RunWithNet() {
            @Override
            public void runWithNet() {
                if (battleFragment.getView() == null) {
                    return;
                }

                FrameLayout frameLayout = (FrameLayout) battleFragment.getView().findViewById(R.id.battle_interface);
                frameLayout.removeAllViews();
                battleFragment.getActivity().getLayoutInflater().inflate(R.layout.fragment_battle_teampreview, frameLayout);
                for (int i = 0; i < team1.size(); i++) {
                    ImageView sprites = (ImageView) battleFragment.getView().findViewById(battleFragment.getTeamPreviewSpriteId("p1", i));
                    PokemonInfo pkm = team1.get(i);
                    Ion.with(sprites)
                            .placeholder(Pokemon.getPokemonSprite(battleFragment.getActivity(), MyApplication.toId(pkm.getName()), true, pkm.isFemale(), pkm.isShiny()))
                            .error(Pokemon.getPokemonSprite(battleFragment.getActivity(), MyApplication.toId(pkm.getName()), true, pkm.isFemale(), pkm.isShiny()))
                            .fitCenter().fadeIn(true)
                            .load(Pokemon.getPokemonSpriteAnimatedRemoteURL(battleFragment.getActivity(), pkm.getName(), false, pkm.isFemale(), pkm.isShiny()));
                    //sprites.setImageResource(Pokemon.getPokemonSprite(battleFragment.getActivity(),
                    //        MyApplication.toId(pkm.getName()), true, pkm.isFemale(), pkm.isShiny()));
                    //patch
                    //Pokemon.getPokemonSpriteAnimatedRemoteURL(battleFragment.getActivity(), pkm.getName(), true, pkm.isFemale(), pkm.isShiny());
                    //end patch
                    ImageView image = (ImageView)battleFragment.getView().findViewById(battleFragment.getIconId("p1", i));
                    //Ion.with(image)
                    //        .error(Pokemon.getPokemonIcon(battleFragment.getActivity(), MyApplication.toId(pkm.getName())))
                    //        .load(Pokemon.getPokemonSpriteAnimatedRemoteURL(battleFragment.getActivity(), pkm.getName(), false, pkm.isFemale(), pkm.isShiny()));
                    /*((ImageView) battleFragment.getView().findViewById(battleFragment.getIconId("p1", i)))
                            .setImageResource(Pokemon.getPokemonIcon(battleFragment.getActivity(),
                                    MyApplication.toId(pkm.getName())));*/
                }
                for (int i = team1.size(); i < 6; i++) {
                    //((ImageView) battleFragment.getView().findViewById(battleFragment.getIconId("p1", i)))
                    //        .setImageResource(R.drawable.pokeball_none);
                }
                for (int i = 0; i < team2.size(); i++) {
                    ImageView sprites2 = (ImageView) battleFragment.getView().findViewById(battleFragment.getTeamPreviewSpriteId("p2", i));
                    PokemonInfo pkm = team2.get(i);
                    Ion.with(sprites2)
                            .placeholder(Pokemon.getPokemonSprite(battleFragment.getActivity(), MyApplication.toId(pkm.getName()), false, pkm.isFemale(), pkm.isShiny()))
                            .error(Pokemon.getPokemonSprite(battleFragment.getActivity(), MyApplication.toId(pkm.getName()), false, pkm.isFemale(), pkm.isShiny()))
                            .fitCenter().fadeIn(true)
                            .load(Pokemon.getPokemonSpriteAnimatedRemoteURL(battleFragment.getActivity(), pkm.getName(), true, pkm.isFemale(), pkm.isShiny()));
                    //sprites.setImageResource(Pokemon.getPokemonSprite(battleFragment.getActivity(),
                    //        MyApplication.toId(pkm.getName()), false, pkm.isFemale(), pkm.isShiny()));
                    ImageView image = (ImageView)battleFragment.getView().findViewById(battleFragment.getIconId("p2", i));
                    //Ion.with(image)
                    //        .error(Pokemon.getPokemonIcon(battleFragment.getActivity(), MyApplication.toId(pkm.getName())))
                    //        .load(Pokemon.getPokemonSpriteAnimatedRemoteURL(battleFragment.getActivity(), pkm.getName(), false, pkm.isFemale(), pkm.isShiny()));
                    /*((ImageView) battleFragment.getView().findViewById(battleFragment.getIconId("p2", i)))
                            .setImageResource(Pokemon.getPokemonIcon(battleFragment.getActivity(),
                                    MyApplication.toId(pkm.getName())));*/
                }
                for (int i = team2.size(); i < 6; i++) {
                    //((ImageView) battleFragment.getView().findViewById(battleFragment.getIconId("p2", i)))
                    //        .setImageResource(R.drawable.pokeball_none);
                }
                battleFragment.setTeamSize(teamSelectionSizeFinal);
                battleFragment.getView().findViewById(R.id.p2a_prev)
                        .setOnClickListener(battleFragment.new PokemonInfoListener(false, 0));
                battleFragment.getView().findViewById(R.id.p2b_prev)
                        .setOnClickListener(battleFragment.new PokemonInfoListener(false, 1));
                battleFragment.getView().findViewById(R.id.p2c_prev)
                        .setOnClickListener(battleFragment.new PokemonInfoListener(false, 2));
                battleFragment.getView().findViewById(R.id.p2d_prev)
                        .setOnClickListener(battleFragment.new PokemonInfoListener(false, 3));
                battleFragment.getView().findViewById(R.id.p2e_prev)
                        .setOnClickListener(battleFragment.new PokemonInfoListener(false, 4));
                battleFragment.getView().findViewById(R.id.p2f_prev)
                        .setOnClickListener(battleFragment.new PokemonInfoListener(false, 5));

                /*if (battleFragment.getAnimatorSetQueue().isEmpty() && battleFragment.getRequestJson() != null) {
                    battleFragment.startRequest();
                }*/

            }
        });
    }

    public void stopAnimatedTeamPreview() {

    }
}
