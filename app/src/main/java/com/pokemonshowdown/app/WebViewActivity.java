package com.pokemonshowdown.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class WebViewActivity extends Activity {

    private WebView webView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        Bundle b = getIntent().getExtras();
        String url = b.getString("URL");

        webView = (WebView) findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);

        final Toast toast = Toast.makeText(getApplicationContext(), "Loading...", Toast.LENGTH_LONG);
        toast.show();

        // Handle redirects and link clicks
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            //@Override
            public void onProgressChanged(WebView view, int progress) {
                setTitle(String.format("Loading... %d%% complete", progress));
                if (progress == 100) {
                    toast.cancel();
                    setTitle("Showdown!");
                }
            }
        });

        webView.loadUrl(url);
    }
}
