package com.pokemonshowdown.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.text.Html;
import android.util.Log;
import android.view.View;

import com.pokemonshowdown.data.MoveDex;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.MessageFormat;

public class MoveInfoFragment {
    public static boolean showMoveInformation(Activity activity, String moveName, int movePP, int maxMovePP) {
        try {
            JSONObject move = MoveDex.get(activity.getApplication()).getMoveJsonObject(moveName);

            final int accuracy;

            if(move.get("accuracy") instanceof Boolean)
                if (move.getBoolean("accuracy"))
                    accuracy = 100;
                else
                    accuracy = 0;
            else
                accuracy = move.getInt("accuracy");


            Dialog dialog = new AlertDialog.Builder(activity)
                    .setTitle(move.getString("name"))
                    .setMessage(Html.fromHtml(
                            MessageFormat.format(
                                    (movePP == -1 ? "<b>MAX PP</b>  <p><small>{1}</small></p>" : "<b>PP</b>\n  <p><small>{0}/{1}</small></p>") +
                                    "<b>BASE POWER</b>  <p><small>{2}</small></p>" +
                                    "<b>ACCURACY</b>  <p><small>{3}</small></p>" +
                                    "<b>PRIORITY</b>  <p><small>{4}</small></p>" +
                                    "<b>CATEGORY</b>  <p><small>{5}</small></p>" +
                                    "<b>DESCRIPTION</b>  <p><small>{6}</small></p>",
                                    movePP == -1 ? 0 : movePP, maxMovePP == -1 ? move.getString("pp") : maxMovePP,
                                    move.getString("basePower"),
                                    accuracy,
                                    move.getInt("priority"),
                                    move.getString("category"),
                                    move.getString("desc"))
                            )
                    )
                    .setIcon(MoveDex.getTypeIcon(activity.getApplication(), move.getString("type")))
                    .create();
            dialog.show();
        } catch (JSONException e) {
            Log.w("MoveInfoFragment", "JSON Exception: " + e.getStackTrace());
            e.printStackTrace();
        }
        return true;
    }

    public static boolean showMoveInformation(Activity activity, String moveName) {
        return showMoveInformation(activity, moveName, -1, -1);
    }
}
